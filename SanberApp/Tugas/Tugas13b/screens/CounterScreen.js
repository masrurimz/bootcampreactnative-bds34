import React from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment, incrementByAmount } from "../redux/counterSlice";

export default function CounterScreen() {
	const count = useSelector((state) => state.counter.value);
	const dispatch = useDispatch();

	return (
		<View>
			<View>
				<Button
					title="Increment"
					onPress={() => {
						console.log(increment());
						dispatch(increment());
					}}
				/>
				<Text>{count}</Text>
				<Button
					title="Decrement"
					onPress={() => {
						console.log(decrement());
						dispatch(decrement());
					}}
				/>
				<Button
					title="Increment by 10"
					onPress={() => {
						console.log(incrementByAmount(10));
						dispatch(incrementByAmount(10));
					}}
				/>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({});
