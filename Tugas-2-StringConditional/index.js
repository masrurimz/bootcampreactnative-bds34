// var sayHello = "Hello World!";
// console.log(sayHello);

// var name = "John"; // Tipe
// var angka = 12;
// var todayIsFriday = false;

// console.log(name); // "John"
// console.log(angka); // 12
// console.log(todayIsFriday); // false

// var items;
// console.log(items); // Undefined

// console.log(8 + "8");
// console.log(8 + 8);

// var angka = 8;
// console.log(angka == "8"); // true, padahal "8" adalah string.
// console.log(angka === "8"); // false, karena tipe data nya berbeda
// console.log(angka === 8); // true

// var word = "JavaScript is Awesome";
// console.log(word.length);
// console.log(word.charAt(0));
// console.log(word.charAt);

// var string1 = "good";
// var string2 = "luck";
// console.log(string1.concat(string2, " sobat ", "sanber ")); // goodluck

// var car1 = "Lykan Hypersport Car";
// var car2 = car1.substr(6, 10);
// var car3 = car1.substring(6, 16);

// console.log(car2); // Hypersport Car
// console.log(car3); // Hypersport

// var mood = "blue";

// if (mood === "happy") {
// 	console.log("hari ini aku bahagia!");
// } else if (mood === "blue") {
// 	console.log("hari ini aku sedang sedih");
// } else {
// 	console.log("hari ini aku tidak bahagia");
// }

// var minimarketStatus = "open";

// var telur = "soldout";
// var buah = "soldout";

// if (minimarketStatus === "open") {
// 	console.log("saya akan membeli telur dan buah");
// 	if (telur === "soldout" || buah === "soldout") {
// 		console.log("belanjaan saya tidak lengkap");
// 	} else if (telur === "soldout") {
// 		console.log("telur habis");
// 	} else if (buah === "soldout") {
// 		console.log("buah habis");
// 	}
// } else {
// 	console.log("minimarket tutup, saya pulang lagi");
// }

// var buttonPushed = 10;

// switch (buttonPushed) {
// 	case 1: {
// 		console.log("matikan TV!");
// 		break;
// 	}
// 	case 2: {
// 		console.log("turunkan volume TV!");
// 		break;
// 	}
// 	case 3: {
// 		console.log("tingkatkan volume TV!");
// 		break;
// 	}
// 	case 4: {
// 		console.log("matikan suara TV!");
// 		break;
// 	}
// 	default: {
// 		console.log("Tidak terjadi apa-apa");
// 	}
// }

// var age = 100;

// var voteable;
// if (age < 18) {
// 	voteable = "Too Young";
// } else {
// 	voteable = "Old Enough";
// }

// var voteable = age < 18 ? "Too young" : age > 60 ? "Too Old" : "Old Enough";

// console.log(voteable);

// var isRain = false;
// var isCold = false;

// if (isCold && isRain) {
// 	console.log("Wear Jacket", "Use Umbrela");
// } else if (!isCold && isRain) {
// 	console.log("Use Umbrela");
// } else {
// 	console.log("Wear Shirt");
// }

// var nama = null;

// // if (!Boolean(nama)) {
// if (!nama) {
// 	console.log("Nama belum diisi");
// }
