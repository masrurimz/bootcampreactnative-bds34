// var flag = 1;

// while (flag < 10) {
// 	console.log("Iterasi ke-", flag);

// 	flag++;
// }

// var flagDoWhile = 10;

// do {
// 	console.log("Iterasi dowhile ke-", flagDoWhile);

// 	flagDoWhile++;
// } while (flagDoWhile < 10);

// for (var flag = 1; flag < 10; flag++) {
// 	console.log("Iterasi for ke-", flag);
// }

// for (var flag = 10; flag > 1; flag--) {
// 	console.log("Iterasi for ke-", flag);
// }

// flag = flag - 3
// for (var flag = 30; flag > 1; flag -= 3) {
// 	console.log("Iterasi for ke-", flag);
// }

// flag = flag - 3
// for (var flag = 0; flag < 30; flag += 3) {
// 	console.log("Iterasi for ke-", flag);
// }

// for (var flag = 1; flag < 10; flag++) {
// 	var isDividedByThree = flag % 3 === 0;

// 	if (isDividedByThree) {
// 		continue;
// 	}

// 	console.log("Iterasi for ke-", flag);
// }

// var sentence = "Hello World!!";
// var searchChar = "o";

// for (var index = 0; index < sentence.length; index++) {
// 	var currentCharacter = sentence[index];

// 	if (currentCharacter.toLocaleLowerCase() === searchChar) {
// 		console.log("Index dari ", searchChar, " adalah ", index);
// 		break;
// 	}

// 	console.log(index, sentence[index]);
// }

// var height = 8;
// var widht = 3;

// var output = ""; // Variable untuk menampung output

// for (var row = 0; row < height; row++) {
// 	for (var col = 0; col < widht; col++) {
// 		output += "#"; // Menambahkan karakter ke variable
// 	}
// 	output += "\n"; // Menambahkan baris baru
// }

// console.log(output);

// function tampilkan() {
// 	// console.log("halo!");

// 	return 5;
// }

// console.log(tampilkan());

// var hasil = tampilkan();
// console.log(hasil);

// function kalikan(a, b) {
// 	return a * b;
// }

// console.log(kalikan(10, 7));

// var hasil = kalikan(9, 11);
// console.log("nilai dari hasil", hasil);

// function jumlahkan(a, b = 1) {
// 	console.log({ a, b });

// 	return a + b;
// }
// console.log("hasil jumlahkan", jumlahkan(7));

// console.log("hasil jumlahkan", jumlahkan(7, 10));

// function kurangi(a, b) {
// 	return a - b;
// }

// var minus = kurangi;

// var min = function (a, b) {
// 	return a - b;
// };

// console.log(kurangi(10, 3));
// console.log(minus(10, 3));
// console.log(min(10, 3));

// var kalikan = function (a, b) {
// 	return a * b;
// };

// var jumlahkan = function (a, b) {
// 	return a + b;
// };

// var operasikan = function (a, b, operasi) {
// 	var hasil = operasi(a, b);

// 	return "Hasil operasi dari " + a + " dan " + b + " adalah " + hasil;
// };

// console.log(operasikan(10, 7, kalikan));
// console.log(operasikan(10, 7, jumlahkan));
// console.log(
// 	operasikan(10, 7, function (x, y) {
// 		return x - y;
// 	}),
// );

// Pure Function
// Menerima input dari parameter dan mengeluarkan output dari return
// Tidak ada side effect

// var a = 10;
// var b = 7;
// var hasil;

// var tambahkanBukanPure = function () {
// 	hasil = a + b;

// 	console.log(hasil);
// };

// tambahkanBukanPure();

var convertMonth = function (tanggal, bulan, tahun) {
	switch (bulan) {
		case 2: {
			console.log(+tanggal + " Februari " + tahun);
			break;
		}
		case 3: {
			console.log(+tanggal + " Maret " + tahun);
			break;
		}
		case 4: {
			console.log(+tanggal + " April " + tahun);
			break;
		}
		case 5: {
			console.log(+tanggal + " Mei " + tahun);
			break;
		}
		case 6: {
			console.log(+tanggal + " Juni " + tahun);
			break;
		}
		case 7: {
			console.log(+tanggal + " Juli " + tahun);
			break;
		}
		case 8: {
			console.log(+tanggal + " Agustus " + tahun);
			break;
		}
		case 9: {
			console.log(+tanggal + " September " + tahun);
			break;
		}
		case 10: {
			console.log(+tanggal + " Oktober " + tahun);
			break;
		}
		case 11: {
			console.log(+tanggal + " November " + tahun);
			break;
		}
		case 12: {
			console.log(+tanggal + " Desember " + tahun);
			break;
		}
		default: {
			console.log(+tanggal + " Januari " + tahun);
		}
	}
};

convertMonth(20, 3, 2022);
convertMonth(1, 7, 1998);
