// var numbers = [0, 1, 2, 3];

// numbers.push(4);
// console.log(numbers);

// numbers.push(5, 6, 7);
// console.log(numbers);

// numbers.pop();
// console.log(numbers);
// numbers.pop();
// console.log(numbers);

// var numbers = [0, 1, 2, 3];

// numbers.unshift(-1);
// console.log(numbers);

// numbers.unshift(-4, -3);
// console.log(numbers);

// numbers.shift();
// numbers.shift();
// console.log(numbers);

// var numbers = [12, 1, 3];
// numbers.sort();
// console.log(numbers); // [1, 12, 3]

// numbers.sort(function (a, b) {
// 	return a - b;
// 	// 12 - 1 => 1 (positif) => b lebih kecil dari a
// 	// b akan ditempatkan sebelah kiri a
// 	// [12, 1, 3] => [1, 12, 3]
// });
// console.log(numbers);

// var angka = [0, 1, 2, 3, 4];
// var hasil = angka.slice(-2);
// console.log(hasil);

// var hasil = angka.slice(1, -2);
// console.log(hasil);

// console.log(angka);

// var fruits = ["apple", "banana", "kiwi", "mellon"];

// // fruits.splice(1, 2);
// fruits.splice(1, 0, "pineapple", "grape");
// // fruits.splice(1, 2, "pineapple", "grape");
// console.log(fruits);

// function range(startNum, finishNum) {
// 	if (!startNum || !finishNum) {
// 		return -1;
// 	}

// 	return [];
// }

// console.log(range(10));
