// var name = "hilmy";

// {
// 	var name = "Firdaus";
// 	console.log(name);
// }

// var name = "Achmad";

// console.log(name);

// let name = "hilmy";

// {
// 	let name = "Firdaus";
// 	console.log(name);
// }

// let name = "Achmad"; //error

// console.log(name);

// const person = {
// 	id: 1,
// 	name: "john",
// };

// // Error
// person = {
// 	id: 2,
// 	name: "wick",
// };

// person.name = "Wick";
// console.log(person);

// // Arrow Function
// function jumlahkanBiasa(a, b) {
// 	return a + b;
// }

// const jumlahkanArrow = (a, b) => {
// 	return a + b;
// };

// const jumlahkanArrowShortReturn = (a, b) => a + b;

// const jumlahkanArrowShortReturnObj = (a, b) => ({ a, b, hasil: a + b });

// console.log(jumlahkanBiasa(10, 7));
// console.log(jumlahkanArrow(10, 7));
// console.log(jumlahkanArrowShortReturn(10, 7));
// console.log(jumlahkanArrowShortReturnObj(10, 7));

// // String Template Literals
// const name = "Hilmy";
// const age = 24;

// const textNotEs6 = "Nama saya adalah " + name + " dan usia saya adalah " + age;
// console.log("Bukan ES6", textNotEs6);

// const textEs6 = `Nama saya adalah ${name} dan usia saya adalah ${age}`;
// console.log("ES6", textEs6);

// const person = {
// 	age: 25,
// 	firstName: "John",
// 	lastName: "Wick",
// 	["hair Color"]: "Black",
// 	cars: ["Bugati"],
// };

// // Not ES6
// console.log(person.age);
// console.log(person.firstName);
// console.log(person.lastName);

// // ES6
// const { firstName, age, ...otherProps } = person;
// console.log(age);
// console.log(firstName);
// console.log(otherProps);

// // ES6 Array
// const cars = ["Lambo", "Ferrari", "Aston Martin"];
// const [, , car3] = cars;
// console.log(car3);

// // Joining Object Property
// const person1 = {
// 	firstName: "John",
// 	age: 30,
// };

// const person2 = {
// 	firstName: "Wick",
// 	skinColor: "White",
// };

// const person3 = {
// 	...person1,
// 	...person2,
// 	age: 25,
// };
// console.log(person3);

// const firstName = "John";
// const lastName = "Wick";
// const age = 40;

// const personNotEs6 = {
// 	age: age,
// 	firstName: firstName,
// 	lastName: lastName,
// };

// const personEs6 = {
// 	age,
// 	firstName,
// 	lastName,
// };

// console.log({ personEs6, personNotEs6 });
