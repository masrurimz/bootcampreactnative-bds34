// var personArr = ["John", "Doe", "male", 27];

// var personObj = {
// 	firstName: "John",
// 	lastName: "Doe",
// 	gender: "male",
// 	age: 27,
// };

// console.log(personArr[0]); // John
// console.log(personObj.firstName); // John

// var propertyKey = "origin" + 10;
// var propertyValue = "Indonesia";

// var person = {
// 	name: "John",
// 	age: 10,
// 	clothes: {
// 		upper: "shirt",
// 		lower: "shoes",
// 	},
// 	["hair color"]: "red",
// 	// origin: "Indonesia",
// 	[propertyKey]: propertyValue,
// 	greetings: function () {
// 		console.log("Hello!!!");
// 	},
// 	car: ["Lamborghini"],
// };

// console.log(person);
// console.log("clothes", person.clothes);
// console.log("hair color", person["hair color"]);
// console.log(propertyKey, person[propertyKey]);

// person.greetings();

// console.log(person.car);

// person.car.push("Ferrari", "Porche");
// console.log(person.car);

// person.name = "Wick";
// console.log(person);

// person.houseCount = 5;
// console.log(person);

// var car = {};
// console.log({ car });

// car.color = "Red";
// console.log({ car });

// var tugas1 = {
// 	"1. Abduh Muhamad": {
// 		firstName: "Abduh",
// 		lastName: "Muhamad",
// 		gender: "male",
// 		age: 28,
// 	},
// 	"2. Ahmad Taufik": {
// 		firstName: "Ahmad",
// 		lastName: "Taufik",
// 		gender: "male",
// 		age: 35,
// 	},
// };
// console.log(tugas1);

// var sales = [
// 	{ name: "Sepatu Stacattu", price: 1500000 },
// 	{ name: "Baju Zoro", price: 500000 },
// 	{ name: "Baju H&N", price: 250000 },
// 	{ name: "Sweater Uniklooh", price: 175000 },
// 	{ name: "Casing Handphone", price: 50000 },
// ];

// console.log(sales);

// function arrayToObject(arr) {
// 	// Code di sini
// 	const output = {};

// 	for (let index = 0; index < arr.length; index++) {
// 		const personArr = arr[index];

// 		const firstName = personArr[0];
// 		const lastName = personArr[1];
// 		const gender = personArr[2];
// 		const yearBorn = personArr[3];

// 		const dateNow = new Date();
// 		const thisYear = dateNow.getFullYear(); // 2022 (tahun sekarang)

// 		const age = thisYear - yearBorn;

// 		const person = {
// 			firstName,
// 			lastName,
// 			gender,
// 			age,
// 		};

// 		output[`${index + 1}. ${firstName} ${lastName}`] = person;
// 	}

// 	console.log(output);

// 	return output;
// }

// // Driver Code
// var people = [
// 	["Bruce", "Banner", "male", 1975],
// 	["Natasha", "Romanoff", "female"],
// ];
// arrayToObject(people);
// /*
// {
//   '1. Bruce Banner': {
//     firstName: 'Bruce',
//     lastName: 'Banner',
//     gender: 'male',
//     age: 45
//   },
//   '2. Natasha Romanoff': {
//     firstName: 'Natasha',
//     lastName: 'Romanoff',
//     gender: 'female',
//     age: 'Invalid Birth Year'
//   }
// }
// */
