// // Request 1
// console.log("Ambil data pesan chat"); //request ke server

// console.log("Ambil data profil"); // request ke server
// // Req 2 selesai
// // menjalankan perintah : callback function

// // Request 2
// console.log('Ambil data feed/reels'); // request ke server

// setTimeout(() => {
// 	console.log("Saya dijalankan no 2");
// }, 3000);

// console.log("Saya dijalankan duluan");

// console.log("Request Data Chat");
// setTimeout(() => {
// 	console.log("Data Chat diterima");

// 	console.log("Request Data Profil");
// 	setTimeout(() => {
// 		console.log("Data Profil Diterima");
// 	}, 1000);
// }, 3000);

// console.log("Request Data Feed");
// setTimeout(() => {
// 	console.log("Data Feed diterima");
// }, 2000);

// console.log("SALAH Request Data Profil");
// setTimeout(() => {
// 	console.log("SALAH Data Profil Diterima");
// }, 1000);

// // Contoh Callback
// const periksaDokter = (nomerAntri, callback) => {
// 	if (nomerAntri > 50) {
// 		callback(false, nomerAntri);
// 	} else if (nomerAntri < 10) {
// 		callback(true, nomerAntri);
// 	}
// };

// periksaDokter(64, (check, nomerAntri) => {
// 	if (check) {
// 		console.log("Sebentar lagi giliran saya");
// 		console.log("Sekarang nomor antiran ke-", nomerAntri);
// 	} else {
// 		console.log("Saya jalan-jalan dulu");
// 		console.log("Sekarang nomor antiran ke-", nomerAntri);
// 	}
// });

// const willIGetNewPhone = (isMomHappy) =>
// 	new Promise((resolve, reject) => {
// 		setTimeout(() => {
// 			if (isMomHappy) {
// 				const phone = {
// 					brand: "Samsung",
// 					color: "Black",
// 				};

// 				resolve({ phone, isMomHappy });
// 			} else {
// 				const reason = new Error("Mom is not happy");

// 				reject(reason);
// 			}
// 		}, 1000);
// 	});

// willIGetNewPhone(false)
// 	.then(({ phone, isMomHappy }) => {
// 		console.log("I have a new phone", phone);
// 		console.log("Because isMomHappy is ", isMomHappy);
// 	})
// 	.catch((e) => {
// 		console.error("Promise Rejected", e.message);
// 	})
// 	.finally(() => {
// 		console.log("Finally selalu dieksekusi");
// 	});

// console.log("Saya dieksekusi duluan");

// Async Await
// const willIGetNewPhone = (isMomHappy, id) =>
// 	new Promise((resolve, reject) => {
// 		if (isMomHappy) {
// 			const phone = {
// 				id,
// 				brand: "Samsung",
// 				color: "Black",
// 			};

// 			resolve({ phone, isMomHappy });
// 		} else {
// 			const reason = new Error("Mom is not happy");

// 			reject(reason);
// 		}
// 	});

// willIGetNewPhone(true, 1)
// 	.then(({ phone, isMomHappy }) => {
// 		console.log("I have a new phone", phone);

// 		willIGetNewPhone(true, 2)
// 			.then(({ phone, isMomHappy }) => {
// 				console.log("I have a new phone", phone);

// 				willIGetNewPhone(true, 3)
// 					.then(({ phone, isMomHappy }) => {
// 						console.log("I have a new phone", phone);

// 						willIGetNewPhone(true, 4)
// 							.then(({ phone, isMomHappy }) => {
// 								console.log("I have a new phone", phone);
// 							})
// 							.catch((e) => {
// 								console.error("Promise Rejected", e.message);
// 							})
// 							.finally(() => {
// 								console.log("Finally selalu dieksekusi");
// 							});
// 					})
// 					.catch((e) => {
// 						console.error("Promise Rejected", e.message);
// 					})
// 					.finally(() => {
// 						console.log("Finally selalu dieksekusi");
// 					});
// 			})
// 			.catch((e) => {
// 				console.error("Promise Rejected", e.message);
// 			})
// 			.finally(() => {
// 				console.log("Finally selalu dieksekusi");
// 			});
// 	})
// 	.catch((e) => {
// 		console.error("Promise Rejected", e.message);
// 	})
// 	.finally(() => {
// 		console.log("Finally selalu dieksekusi");
// 	});

// const runMe = async () => {
// 	try {
// 		let result;

// 		result = await willIGetNewPhone(true, 1);
// 		console.log("I have a new phone", result.phone);

// 		result = await willIGetNewPhone(false, 2);
// 		console.log("I have a new phone", result.phone);

// 		result = await willIGetNewPhone(true, 3);
// 		console.log("I have a new phone", result.phone);

// 		result = await willIGetNewPhone(true, 4);
// 		console.log("I have a new phone", result.phone);
// 	} catch (error) {
// 		console.error("Promise Rejected", error.message);
// 	} finally {
// 		console.log("Finally selalu dieksekusi");
// 	}
// };

// runMe();
