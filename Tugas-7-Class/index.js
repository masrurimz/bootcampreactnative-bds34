// const greetings = () => {
// 	console.log("Hello");
// };

// class Vechile {}

// const Kendaraan = class {};

// function kalikan(a, b) {
// 	return a + b;
// }

// class Vechile {
// 	constructor(brand) {
// 		console.log("Class di instance, dan menghasilkan object");
// 		this._wheels = 4;
// 		this._isDiesel = true;
// 		this._brand = brand;
// 	}

// 	present(greeting) {
// 		return `${greeting}, I have a ${this._brand}`;
// 	}

// static hello() {
// 	return "Hello!!";
// }
// }

// const johnson = new Vechile("Volvo");
// console.log(johnson);
// console.log(johnson.present("Hello"));

// console.log(Vechile.hello());
// console.log(johnson.hello);

// class Car extends Vechile {
// 	constructor(brand, model) {
// 		super(brand);
// 		this._model = model;
// 		this._isDiesel = false;
// 	}

// 	show() {
// 		return `${this.present("Hello")}, it is a ${this._model}`;
// 	}
// }

// const myCar = new Car("Ford", "Mustang");

// console.log(myCar);
// console.log(myCar.show());

// class MotorCycle extends Vechile {
// 	constructor(brand, model) {
// 		super(brand);
// 		this._model = model;
// 		this._wheels = 2;
// 	}

// 	show() {
// 		return `${this.present("Hello")}, it is a ${this._model}`;
// 	}
// }
// const myMotorCycle = new MotorCycle("Piaggio", "Vespa");
// console.log(myMotorCycle);

// class Car extends Vechile {
// 	constructor(brand, model) {
// 		super(brand);
// 		this._model = model;
// 		this._isDiesel = false;
// 	}

// 	get model() {
// 		return this._model.toUpperCase();
// 	}

// 	set model(input) {
// 		this._model = input[0].toUpperCase() + input.slice(1).toLowerCase();
// 	}
// }

// const myCar = new Car("Ford", "Mustang");
// console.log(myCar);
// console.log(myCar._model);
// console.log(myCar.model);

// myCar.model = "feRRarI";
// console.log(myCar);
// console.log(myCar.model);

// function Clock({ template }) {
// 	var timer;

// 	function render() {
// 		var date = new Date();

// 		var hours = date.getHours();
// 		if (hours < 10) hours = "0" + hours;

// 		var mins = date.getMinutes();
// 		if (mins < 10) mins = "0" + mins;

// 		var secs = date.getSeconds();
// 		if (secs < 10) secs = "0" + secs;

// 		var output = template
// 			.replace("h", hours)
// 			.replace("m", mins)
// 			.replace("s", secs);

// 		console.log(output);
// 	}

// 	this.stop = function () {
// 		clearInterval(timer);
// 	};

// 	this.start = function () {
// 		render();
// 		timer = setInterval(render, 1000);
// 		// timer = setInterval(() => render(), 1000);
// 		// timer = setInterval(render.bind(this), 1000);
// 	};
// }

// var clock = new Clock({ template: "h:m:s" });
// clock.start();
